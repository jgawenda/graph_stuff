#ifndef _GRAPH_H_
#define _GRAPH_H_

#include "TileMap.h"
#include <vector>
#include <utility>

/* enum TileType {BAD_TILE, GOOD_TILE}; */

class Node
{
	public:
		/* Node(); // default constructor */ 
		Node(int node_id, int node_value, int x_coord, int y_coord);
		Node(int node_value, int x_coord, int y_coord);
		/* ~Node(); // default destructor */
 
		int getId();
		int getValue();
		std::pair<int, int> getCoords();
		/* Node* getNext(); // get pointer to the next node */
		/* Node* getPrevious(); // get pointer to the previous node */
	protected:
	private:
		static int m_num_of_nodes; // used for automatic generation?

		int m_id; // id of the node?
		int m_value; // BAD_TILE = 0, GOOD_TILE = 1

		/* int m_x_coord; // x coordinate (corresponding to the map) */
		/* int m_y_coord; // y coordinate (corresponding to the map) */
		std::pair<int, int> m_coords;

		/* int m_tile_type; // BAD_TILE = 0, GOOD_TILE = 1 */
		/* bool m_visited; // check if already visited */

		/* Node* m_next_node; // pointer to the next node */
		/* Node* m_previous_node; // pointer to the previous node */
};

class Edge
{
	public:
		/* Edge(); */
		Edge(int start_id, int end_id); // create an edge with given start/end
		/* ~Edge(); */

		int getStart();
		int getEnd();
		std::pair<int, int> getPoints();

	protected:
	private:
		// int m_veight; // weight of an edge
		/* int m_start; // id of the starting node */
		/* int m_end; // id of the end node */
		std::pair<int, int> m_points;
};

class Graph
{
	public:
		Graph();
		Graph(TileMap tile_map, int start_x, int start_y); 
		// could be different, I dunno
		/* ~Graph(); */

		// methods, like checking if some node/edge exists already
		bool findNode(int node_id); // check if node with given id exists
		bool findNode(int x_coord, int y_coord); // check if node with given coordinates exists
		bool findEdge(int start_id, int end_id); // check if edge with given start and end exists

		// adders
		bool addNode(Node node); // add node passed as an argument (copies)
		bool addNode(int node_value, int x_coord, int y_coord); // construct node and add it
		bool addEdge(Edge edge); // add edge passed as an argument (copies)
		bool addEdge(int start_id, int end_id); // construct edge object

		// getters
		int getSize(); // get size of a graph (number of nodes)
		Node* getNode(int node_id); // get node with a given ID
		Node* getNode(int x_coord, int y_coord); // get node with given coords
		Edge* getEdge(int start_id, int end_id);
	protected:
	private:
		/* Node* m_root; // root of the graph/tree */
		std::vector<Node> m_nodes; // list of nodes in a graph
		std::vector<Edge> m_edges; // list of edges in a graph
};

#endif
