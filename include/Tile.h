/**
 * @file Tile.h
 * @author Jakub Gawenda
 */

#ifndef _TILE_H_
#define _TILE_H_

enum TileType {BAD_TILE, GOOD_TILE};

class Tile
{
	public:
		/**
		 * @brief Constructor for Tile class
		 * @param value assigns value to the Tile
		 */
		Tile(int value);

		/**
		 * @brief sets m_visited to true
		 */
		void setVisit();

		/**
		 * @brief gets value of a tile
		 */
		int getValue(); // get value (means it was visited?);
		/**
		 * @brief checks if the tile was already visited
		 * @return true if visited, false if wasn't
		 */
		bool visited();
	protected:
	private:
		int m_value; // value (good or bad)
		bool m_visited; // check if visited
};

#endif
