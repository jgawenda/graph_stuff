#ifndef _TILE_MAP_H_
#define _TILE_MAP_H_

#include "Tile.h"
#include <utility>
#include <vector>

class TileMap
{
	public:
		/**
		 *	@brief constructs a TileMap object from a given 2D vector
		 *	@param tile_vector 2D vector consisting of ones and zeroes
		 */
		TileMap(std::vector<std::vector<int>> tile_vector);

		/* Tile* getTile(int x_coord, int y_coord); */
		/**
		 *	@brief gets a tile corresponding to given pair of coordinates
		 *	@param coords pair of row and col
		 *	@return pointer to Tile object at those coordinates
		 */
		Tile* getTile(std::pair<int, int> coords);

		/**
		 *	@brief gets a tile corresponding to given coordinates
		 *	@param row row of the tile
		 *	@param col column of the tile
		 *	@return pointer to Tile object at those coordinates
		 */
		Tile* getTile(int row, int col);

		/**
		 *	@brief gets all tiles adjacent to given coordinates by side (with value 1)
		 *	@param coords coordinates of the center tile
		 *	@return vector of pairs of coordiantes of tiles
		 */
		std::vector<std::pair<int, int> > getAdjacent(std::pair<int, int> coords);

		/**
		 *	@brief returns number of rows in a map
		 */
		int getRows();

		/**
		 *	@brief returns number of columns in a map
		 */
		int getCols();
	protected:
	private:
		/**
		 *	a 2D vector containing all tiles
		 */
		std::vector<std::vector<Tile>> m_map;
};

#endif
