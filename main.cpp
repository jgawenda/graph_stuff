#include <Graph.h>
#include <TileMap.h>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void showData(std::vector<std::vector<int>> data)
{
	int M = data.size();

	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < M; j++)
		{
			std::cout << data[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

std::vector<std::vector<int>> convert(std::vector<int>& data)
{
	std::vector<std::vector<int> > vec;

    int sq = sqrt(data.size());
    for (int i = 0; i < sq; i++) 
	{
		std::vector<int> row;

        for (int j = 0; j < sq; j++) 
		{
            row.push_back(data[j + (i*sq)]);
        }
        vec.push_back(row);
    }

    return vec;
}

std::vector<std::vector<int>> readFile(const std::string file_name){
	std::ifstream file_stream(file_name);
	std::vector<int> data;

    if (!file_stream){
		std::cout << "The file does not exist.";
    }
    int x;

    while (file_stream >> x)
	{
        data.push_back(x);
	}

    file_stream.close();

    return convert(data);
}

/* std::vector<std::vector<int>> readFile(std::string file_name) */
/* { */
/* 	std::string line; */
/* 	std::ifstream file_stream; */

/* 	std::vector<std::vector<int>> tile_vector; */

/* 	file_stream.open(file_name, std::ifstream::in); */

/* 	if ( !file_stream.good() ) */
/* 	{ */
/* 		std::cout << "An error has occured while reading the file.\n"; */
/* 		file_stream.close(); */
/* 		/1* return nullptr; *1/ */
/* 	} */

/* 	while(getline(file_stream, line)) */
/* 	{ */
/* 		std::vector<int> row; */
/* 		for (auto number : line) */
/* 		{ */
/* 			if (number == '0') */
/* 			{ */
/* 				row.push_back(0); */
/* 			} */
/* 			if (number == '1') */
/* 			{ */
/* 				row.push_back(1); */
/* 			} */
/* 		} */

/* 		tile_vector.push_back(row); */
/* 	} */

/* 	return tile_vector; */
/* } */

int main(int argc, char *argv[])
{
	srand(time(NULL));

	int M = 50;
	int biggest_value = 0;
	std::vector<int> row_list;
	std::vector<int> col_list;

	std::vector<std::vector<int>> tile_vector;

	if (argc == 1)
	{
		std::cout << "Not enough arguments. Usage:\n";
		std::cout << "./main <u/d/l/r> - generates a random 50x50 matrix and checks from a given side\n";
		std::cout << "./main <u/d/l/r> <data_path> - gets the matrix from a file under <data_path>\n";
		return -1;
	}

	if (argc > 3)
	{
		std::cout << "Passed too many arguments.\n";
	}

	// get side from argument
	char side = *argv[1]; // first argument

	// generate map if none passed
	if (argc == 2)
	{
		for (int i = 0; i < M; i++)
		{
			std::vector<int> row;
			// loop for creating one row
			for (int j = 0; j < M; j++)
			{
				int value = rand() % 2;	// 0 -- bad tile, 1 -- good tile
				row.push_back(value);
			}

			tile_vector.push_back(row);
		}
		showData(tile_vector);
		M = tile_vector.size();
	}

	if (argc == 3)
	{
		std::string file_name = argv[2];
		std::cout << file_name << std::endl;
		tile_vector = readFile(file_name);
		showData(tile_vector);
		M = tile_vector.size();
	}

	// generate 2D array of Tile objects from 2D int vector
	TileMap tile_map(tile_vector);

	switch (side)
	{
		case 'u':
			row_list.push_back(0);
			for (int j = 0; j < M; j++)
			{
				// generate tree starting from one of the top side tiles
				Graph graph(tile_map, 0, j);

				// check how many nodes are in the tree
				if (graph.getSize() > biggest_value)
				{
					// change biggest possible value
					biggest_value = graph.getSize();
					// clear vector, add column for the tile
					col_list.clear();
					col_list.push_back(j);
				}
				// if the value is the same as the biggest possible
				else if (graph.getSize() == biggest_value)
				{
					// add tile to the vector
					col_list.push_back(j);
				}
			}

			std::cout << "biggest value achieved is: " << biggest_value << std::endl;
			std::cout << "Can be aquired by starting in:\n";
			for (auto col : col_list)
			{
				std::cout << "row " << row_list[0] << ", col " << col << std::endl;
			}
			break;

		case 'd':
			row_list.push_back(M - 1);
			for (int j = 0; j < M; j++)
			{
				// generate tree starting from one of the top side tiles
				Graph graph(tile_map, row_list[0], j);

				// check how many nodes are in the tree
				if (graph.getSize() > biggest_value)
				{
					// change biggest possible value
					biggest_value = graph.getSize();
					// clear vector, add column for the tile
					col_list.clear();
					col_list.push_back(j);
				}
				// if the value is the same as the biggest possible
				else if (graph.getSize() == biggest_value)
				{
					// add tile to the vector
					col_list.push_back(j);
				}
			}

			std::cout << "biggest value achieved is: " << biggest_value << std::endl;
			std::cout << "Can be aquired by starting in:\n";
			for (auto col : col_list)
			{
				std::cout << "row " << row_list[0] << ", col " << col << std::endl;
			}
			break;

		case 'l':
			/* row_list.push_back(M - 1); */
			col_list.push_back(0);
			for (int i = 0; i < M; i++)
			{
				// generate tree starting from one of the top side tiles
				Graph graph(tile_map, i, col_list[0]);

				// check how many nodes are in the tree
				if (graph.getSize() > biggest_value)
				{
					// change biggest possible value
					biggest_value = graph.getSize();
					// clear vector, add column for the tile
					row_list.clear();
					row_list.push_back(i);
				}
				// if the value is the same as the biggest possible
				else if (graph.getSize() == biggest_value)
				{
					// add tile to the vector
					row_list.push_back(i);
				}
			}

			std::cout << "biggest value achieved is: " << biggest_value << std::endl;
			std::cout << "Can be aquired by starting in:\n";
			for (auto row : row_list)
			{
				std::cout << "row " << row << ", col " << col_list[0] << std::endl;
			}
			break;

		case 'r':
			/* row_list.push_back(M - 1); */
			col_list.push_back(M-1);
			for (int i = 0; i < M; i++)
			{
				// generate tree starting from one of the top side tiles
				Graph graph(tile_map, i, col_list[0]);

				// check how many nodes are in the tree
				if (graph.getSize() > biggest_value)
				{
					// change biggest possible value
					biggest_value = graph.getSize();
					// clear vector, add column for the tile
					row_list.clear();
					row_list.push_back(i);
				}
				// if the value is the same as the biggest possible
				else if (graph.getSize() == biggest_value)
				{
					// add tile to the vector
					row_list.push_back(i);
				}
			}

			std::cout << "biggest value achieved is: " << biggest_value << std::endl;
			std::cout << "Can be aquired by starting in:\n";
			for (auto row : row_list)
			{
				std::cout << "row " << row << ", col " << col_list[0] << std::endl;
			}
			break;
	}

	return 0;
}
