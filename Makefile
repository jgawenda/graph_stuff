CC=clang++
CFLAGS=-Wall -I ./include/

main:
	$(CC) $(CFLAGS) -o main main.cpp ./src/*.cpp
	./main

build:
	$(CC) $(CFLAGS) -o main main.cpp ./src/*.cpp

prod:
	$(CC) $(CFLAGS) -O3 -o main main.cpp ./src/*.cpp
