#include "Tile.h"

Tile::Tile(int value)
{
	m_value = value;
	m_visited = false;
}

void Tile::setVisit()
{
	m_visited = true;
}

int Tile::getValue()
{
	/* m_visited = true; */
	return m_value;
}

bool Tile::visited()
{
	return m_visited;
}
