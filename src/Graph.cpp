#include "Graph.h"
#include "TileMap.h"

#include <iostream>
#include <queue>
#include <vector> // maybe?
#include <utility>
#include <assert.h>
/* #include <stddef.h> */

// NODE STUFF

int Node::m_num_of_nodes = 0;

Node::Node(int node_id, int node_value, int x_coord, int y_coord)
{
	m_id = node_id; // assign node id
	m_value = node_value; // assign value
	/* m_x_coord = x_coord; // assign x coord */
	/* m_y_coord = y_coord; // assign y coord */
	m_coords = std::make_pair(x_coord, y_coord);

	m_num_of_nodes = node_id + 1; // move node id
}

Node::Node(int node_value, int x_coord, int y_coord)
{
	m_id = m_num_of_nodes;
	m_num_of_nodes++;

	m_value = node_value;
	/* m_x_coord = x_coord; */
	/* m_y_coord = y_coord; */
	m_coords = std::make_pair(x_coord, y_coord);
}

int Node::getId()
{
	return m_id;
}

int Node::getValue()
{
	return m_value;
}

std::pair<int, int> Node::getCoords()
{
	/* return std::make_pair(m_x_coord, m_y_coord); */
	return m_coords;
}

// EDGE STUFF

Edge::Edge(int start_id, int end_id)
{
	/* m_start = start_id; */
	/* m_end = end_id; */
	m_points = std::make_pair(start_id, end_id);
}

int Edge::getStart()
{
	return m_points.first;
}

int Edge::getEnd()
{
	return m_points.second;
}

std::pair<int, int> Edge::getPoints()
{
	/* return std::make_pair(m_start, m_end); */
	return m_points;
}

// GRAPH STUFF (OH BOY)

Graph::Graph()
{
}

Graph::Graph(TileMap tile_map, int start_row, int start_col)
{
	// basic asserts
	assert(tile_map.getRows() > 0);
	assert(tile_map.getCols() == tile_map.getRows());
	assert(start_row >= 0 && start_row < tile_map.getRows());
	assert(start_col >= 0 && start_col < tile_map.getCols());

	Tile* tile;

	tile = tile_map.getTile(start_row, start_col);

	if ( tile -> getValue() == 0 )
	{
		/* std::cout << "Starting point has a value of 0\n"; */
		return;
	}

	std::queue< std::pair<int, int> > tile_queue; // tile coordinates to check
	std::vector< std::pair<int, int> > adjacent_tiles; // vector for storing adjacent tile coords
	tile_queue.push(std::make_pair(start_row, start_col));

	std::pair<int, int> coords;

	while( !tile_queue.empty() )
	{
		coords = tile_queue.front();
		tile_queue.pop();

		/* std::cout << "Checking tile with coordinates; row: " << coords.first << " col: " << coords.second */
		/* 	<< std::endl; */

		/* if ( tile_map[coords.first][coords.second] == GOOD_TILE ) */
		/* { */
			/* if ( !(this -> findNode(coords.first, coords.second)) ) */
			/* { */
			/* 	this -> addNode(GOOD_TILE, coords.first, coords.second); */
			/* } */
		/* } */
		tile = tile_map.getTile(coords);

		// add to graph (if not already in)
		tile -> setVisit();
		if ( !(this -> findNode(coords.first, coords.second)) )
		{
			this -> addNode(GOOD_TILE, coords.first, coords.second);
		}

		adjacent_tiles = tile_map.getAdjacent(coords);
		/* std::cout << "Not visited adjacent tiles to: row " << coords.first << " col " << coords.second */
		/* 	<< " num: " << adjacent_tiles.size() << std::endl; */

		for (auto tile_coord : adjacent_tiles)
		{
			tile_queue.push(tile_coord);
		}
	}
}

bool Graph::findNode(int node_id)
{
	assert(node_id >= 0);
	for (auto node : m_nodes)
	{
		if (node.getId() == node_id)
		{
			return true; // found node with specific id
		}
	}

	return false; // didn't find node with specific id
}

bool Graph::findNode(int x_coord, int y_coord)
{
	assert(x_coord >= 0);
	assert(y_coord >= 0);

	std::pair<int, int> coords = std::make_pair(x_coord, y_coord);

	for (auto& node : m_nodes)
	{
		if (node.getCoords() == coords)
		{
			return true;
		}
	}

	return false;
}

bool Graph::addNode(Node node)
{
	/* assert(node); */

	m_nodes.push_back(node);

	return true;
}

bool Graph::addNode(int node_value, int x_coord, int y_coord)
{
	assert(node_value >= 0);
	assert(x_coord >= 0);
	assert(y_coord >= 0);

	m_nodes.push_back(Node(node_value, x_coord, y_coord));

	return true;
}

bool Graph::addEdge(Edge edge)
{
	/* assert(edge); */

	m_edges.push_back(edge);

	return true;
}

// GRAPH GETTERS

int Graph::getSize()
{
	return m_nodes.size();
}

// assumes that numbering of nodes is consecutive (i.e. no gaps in numbers).
Node* Graph::getNode(int node_id)
{
	assert(node_id >= 0);
	/* assert(m_nodes.size()); */

	/* for (auto& node : m_nodes) */
	/* { */
	/* 	if (node.getId() == node_id) */
	/* 	{ */
	/* 		return &node; // return address of the node */
	/* 	} */
	/* } */

	if (node_id < m_nodes.size())
	{
		return &m_nodes[node_id];
	}

	return nullptr;

	/* return nullptr; */
}

Node* Graph::getNode(int x_coord, int y_coord)
{
	assert(x_coord >= 0);
	assert(y_coord >= 0);

	std::pair<int, int> coords = std::make_pair(x_coord, y_coord);

	for (auto& node : m_nodes)
	{
		if (node.getCoords() == coords)
		{
			return &node;
		}
	}

	return nullptr;
}

Edge* Graph::getEdge(int start_id, int end_id)
{
	assert(start_id >= 0);
	assert(end_id >= 0);

	std::pair<int, int> points = std::make_pair(start_id, end_id);

	for (auto& edge : m_edges)
	{
		if (edge.getPoints() == points)
		{
			return &edge;
		}
	}
	
	return nullptr;
}
