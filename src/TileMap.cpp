#include "TileMap.h"
#include "Tile.h"
#include <vector>
#include <assert.h>

TileMap::TileMap(std::vector<std::vector<int>> tile_vector)
{
	assert(tile_vector.size() > 0);
	assert(tile_vector[0].size() > 0);
	assert(tile_vector.size() == tile_vector[0].size());

	/* for (auto row : tile_vector) */
	/* { */
	/* 	for (auto column : row ) */
	/* 	{ */
			
	/* 	} */
	/* } */


	for (int i = 0; i < tile_vector.size(); i++)
	{
		std::vector <Tile> row;
		/* m_map.push_back(std::vector<Tile>); */
		for (int j = 0; j < tile_vector[i].size(); j++)
		{
			row.push_back(Tile(tile_vector[i][j]));
			/* m_map[i].push_back(Tile(tile_vector[i][j])); */
		}

		m_map.push_back(row);
	}
}

Tile* TileMap::getTile(std::pair<int, int> coords)
{
	if ( coords.first >= 0 && coords.first < m_map.size() ) 
	{
		if ( coords.second >= 0 && coords.second < m_map[coords.first].size() )
		{
			return &m_map[coords.first][coords.second];
		}
	}

	return nullptr;
}

Tile* TileMap::getTile(int row, int col)
{
	if ( row >= 0 && row < m_map.size() ) 
	{
		if ( col >= 0 && col < m_map[row].size() )
		{
			return &m_map[row][col];
		}
	}

	return nullptr;
}

std::vector<std::pair<int, int> > TileMap::getAdjacent(std::pair<int, int> coords)
{
	std::vector<std::pair<int, int> > adjacent_tiles;

	Tile* tile = nullptr;

	// manual stuff for now
	// get tile from above
	tile = this -> getTile(coords.first - 1, coords.second);
	if (tile)
	{
		if ( !(tile -> visited()) && tile -> getValue() == 1) 
		{
			tile -> setVisit();
			adjacent_tiles.push_back(std::make_pair(coords.first - 1, coords.second));
		}
	}

	// get tile from left
	tile = this -> getTile(coords.first, coords.second - 1);
	if (tile)
	{
		if ( !(tile -> visited()) && tile -> getValue() == 1) 
		{
			tile -> setVisit();
			adjacent_tiles.push_back(std::make_pair(coords.first, coords.second - 1));
		}
	}

	// get tile from right
	tile = this -> getTile(coords.first, coords.second + 1);
	if (tile)
	{
		if ( !(tile -> visited()) && tile -> getValue() == 1) 
		{
			tile -> setVisit();
			adjacent_tiles.push_back(std::make_pair(coords.first, coords.second + 1));
		}
	}

	// get tile from below
	tile = this -> getTile(coords.first + 1, coords.second);
	if (tile)
	{
		if ( !(tile -> visited()) && tile -> getValue() == 1) 
		{
			tile -> setVisit();
			adjacent_tiles.push_back(std::make_pair(coords.first + 1, coords.second));
		}
	}

	return adjacent_tiles;
}

int TileMap::getRows()
{
	return m_map.size();
}

int TileMap::getCols()
{
	return m_map[0].size();
}
